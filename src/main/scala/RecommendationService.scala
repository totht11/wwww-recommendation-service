import akka.actor.ActorSystem
import akka.event.Logging
import akka.http.scaladsl.Http
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.server.Directives._
import akka.stream.ActorMaterializer
import com.emarsys.escher.akka.http.EscherDirectives
import com.emarsys.escher.akka.http.config.EscherConfig
import com.typesafe.config.ConfigFactory
import models.JsonFormats._
import recommender.EventRecommender
import spray.json.DefaultJsonProtocol

object RecommendationService extends App with SprayJsonSupport with DefaultJsonProtocol with EscherDirectives {

  implicit val system = ActorSystem()
  implicit val executor = system.dispatcher
  implicit val materializer = ActorMaterializer()

  implicit val config = ConfigFactory.load()
  implicit val logger = Logging(system, getClass)

  // needed for escher directives
  val escherConfig = new EscherConfig(config.getConfig("escher"))


  val recommender = new EventRecommender()


  val routes = {
    logRequestResult("RecommendationService") {
      path("api" / "Recommendations" / "Events") {
        get {
          escherAuthenticate(List("wwww")) { body =>
            headerValueByName("X-WWWW-UserId") { userId =>
              complete {
                recommender.recommendEvents(userId)
              }
            }
          }
        }
      }
    }
  }


  Http().bindAndHandle(routes, config.getString("http.interface"), config.getInt("http.port"))
}
