package models

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import spray.json.{CollectionFormats, DefaultJsonProtocol}


case class EventCategoryStat(CategoryId: Int, Count: Int)

case class EventCategory(Id: Int)

case class Event(
                  Id: Int,
                  Title: String,
                  OwnerUserId: String,
                  CategoryId: Int,
                  StartTime: String,
                  Duration: Long,
                  DurationUnit: String,
                  Country: String,
                  Description: String,
                  LocationName: String,
                  City: String,
                  Address: String,
                  Created: String,
                  Lat: Double,
                  Lon: Double
                )


object JsonFormats extends SprayJsonSupport with DefaultJsonProtocol with CollectionFormats {
  implicit val eventCategoryStatFormat = jsonFormat2(EventCategoryStat.apply)
  implicit val eventCategoryFormat = jsonFormat1(EventCategory.apply)
  implicit val eventFormat = jsonFormat15(Event.apply)
}
