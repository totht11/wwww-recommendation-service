package recommender

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.client.RequestBuilding
import akka.http.scaladsl.model.{HttpRequest, ResponseEntity}
import akka.http.scaladsl.model.StatusCodes._
import akka.http.scaladsl.unmarshalling.{Unmarshaller, Unmarshal}
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Sink, Source}
import com.emarsys.escher.akka.http.EscherDirectives
import com.emarsys.escher.akka.http.config.EscherConfig
import com.typesafe.config.Config
import models._
import models.JsonFormats._

import scala.concurrent.{ExecutionContext, Future}


class EventRecommender(implicit config: Config, executionContext: ExecutionContext, actorSystem: ActorSystem, actorMaterializer: ActorMaterializer) extends EscherDirectives {

  // needed for escher directives
  val escherConfig = new EscherConfig(config.getConfig("escher"))


  private val random = scala.util.Random


  def recommendEvents(userId: String): Future[Iterable[Event]] = {
    for {
      eventCategories  <- getEventCategories
      eventCategoryStats <- getEventCategoryStats(userId)
      recommendedCategories = calculateRecommendedCategories(eventCategories, eventCategoryStats)
      recommendedEvents <- Future.sequence(loadRecommendedEventsByCategoryAndCount(recommendedCategories))
    } yield {
      recommendedEvents.flatMap(a => a)
    }
  }


  private def loadRecommendedEventsByCategoryAndCount(recommendedCategories: Map[Int, Int]): Iterable[Future[Seq[Event]]] = {
    recommendedCategories.map { case (categoryId, count) => getEventsByCategory(categoryId, count) }
  }

  private def calculateRecommendedCategories(categories: Seq[EventCategory], statistics: Seq[EventCategoryStat]): Map[Int, Int] = {
    val completedStatistics = statistics ++ categories
      .filterNot(c => statistics.exists(s => s.CategoryId == c.Id))
      .map(c => EventCategoryStat(c.Id, 0))

    val categoriesWithWeight = completedStatistics.map(s => (s.CategoryId, s.Count * 10 + 1))

    val recommendedCategories = if (categoriesWithWeight.nonEmpty) (1 to 4).map { _ =>
      categoriesWithWeight
        .map{case (categoryId, weight) => (categoryId, random.nextDouble * weight)}
        .maxBy(_._2)
        ._1
    } else Seq()

    recommendedCategories.groupBy(categoryId => categoryId).mapValues(_.size)
  }


  private def getEventCategories: Future[Seq[EventCategory]] = {
    makeGetRequest[Seq[EventCategory]]("/api/Events/Categories",
      config.getString("services.eventServiceHost"),
      config.getInt("services.eventServicePort"),
      "getting event categories failed"
    )
  }


  private def getEventCategoryStats(userId: String): Future[Seq[EventCategoryStat]] = {
    makeGetRequest[Seq[EventCategoryStat]](s"/api/Users/$userId/PreferredCategories",
      config.getString("services.historyServiceHost"),
      config.getInt("services.historyServicePort"),
      "getting event stats failed"
    )
  }


  private def getEventsByCategory(categoryId: Int, count: Int): Future[Seq[Event]] = {
    makeGetRequest[Seq[Event]](s"/api/Events?CategoryId=$categoryId&limit=$count&withCoords=true",
      config.getString("services.eventServiceHost"),
      config.getInt("services.eventServicePort"),
      "getting events failed"
    )
  }


  private def makeGetRequest[T](uri: String, host: String, port: Int, errorMessage: String)(implicit um: Unmarshaller[ResponseEntity, T]): Future[T] = {
    signRequest("wwww").apply(RequestBuilding.Get(s"http://$host:$port$uri")).flatMap { request =>
      Source.single(request)
        .via(Http().outgoingConnection(host, port))
        .runWith(Sink.head)
        .flatMap { response =>
          response.status match {
            case OK =>
              Unmarshal(response.entity).to[T]
            case _ =>
              Future.failed(new scala.Exception(errorMessage))
          }
        }
    }
  }

}
