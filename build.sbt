enablePlugins(JavaAppPackaging)

name         := "recommendation-service"
organization := "com.hu.bme.aut.rw1ur6"
version      := "1.0"
scalaVersion := "2.11.7"

scalacOptions := Seq("-unchecked", "-deprecation", "-encoding", "utf8")

resolvers += "Escher repo" at "https://raw.github.com/tt0th/escher-akka-http/master/releases"

libraryDependencies ++= {
  val akkaV       = "2.4.1"
  val akkaStreamV = "2.0.1"
  Seq(
    "com.typesafe.akka" %% "akka-actor"                           % akkaV,
    "com.typesafe.akka" %% "akka-stream-experimental"             % akkaStreamV,
    "com.typesafe.akka" %% "akka-http-core-experimental"          % akkaStreamV,
    "com.typesafe.akka" %% "akka-http-experimental"               % akkaStreamV,
    "com.typesafe.akka" %% "akka-http-spray-json-experimental"    % akkaStreamV,
    "com.typesafe.akka" %% "akka-http-testkit-experimental"       % akkaStreamV,
    "com.emarsys"       %  "escher"                               % "0.3",
    "com.emarsys"       %% "escher-akka-http"                     % "0.0.7"
  )
}

Revolver.settings


fork in run := true
